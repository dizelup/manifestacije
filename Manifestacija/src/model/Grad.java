package model;

public class Grad {
	
	private int id;
	private String naziv;
	private int ptt;
	
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public int getPtt() {
		return ptt;
	}
	public void setPtt(int ptt) {
		this.ptt = ptt;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "Grad [id=" + id + ", naziv=" + naziv + ", ptt=" + ptt + "]";
	}
	
	
}
