package model;

public class Manifestacija {

	private int id;
	private String naziv;
	private int brojPosetilaca;
	private Grad grad;
	
	
	@Override
	public String toString() {
		return "Manifestacija [id=" + id + ", naziv=" + naziv + ", brojPosetilaca=" + brojPosetilaca + ", grad=" + grad
				+ "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public int getBrojPosetilaca() {
		return brojPosetilaca;
	}
	public void setBrojPosetilaca(int brojPosetilaca) {
		this.brojPosetilaca = brojPosetilaca;
	}
	public Grad getGrad() {
		return grad;
	}
	public void setGrad(Grad grad) {
		this.grad = grad;
	}
	
	public String toFileRepresentation() {
		return "<" + naziv + "><" + brojPosetilaca + ">";
	}
}
