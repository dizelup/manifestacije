package funkcionalnost;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.plaf.synth.SynthSpinnerUI;
import javax.swing.plaf.synth.SynthToggleButtonUI;

import baza.ConnectionManager;
import model.Grad;
import model.Manifestacija;

public class Test {

	public static void menu() {

		System.out.println("********************************");
		System.out.println("1.Prikaz svih gradova");
		System.out.println("2.Prikaz kulturnih manifestacija");
		System.out.println("3.Pretraga kulturne manifestacije po identifikatoru");
		System.out.println("4.Izmena naziva manifestacije");
		System.out.println("5.Napravi izvestaj za manifestacije");
		System.out.println("6.Ocitaj izvezstaj manifestacije");
		System.out.println("7.Izmeni manifestaciju");
		System.out.println("8.Izmeni grad");
		System.out.println("********************************");
	}
	
	public static List<Grad> ispisSvihGradova(Statement stmt) throws SQLException {
		String sql = "SELECT * FROM manifestacije.grad;";
		ResultSet rset = stmt.executeQuery(sql);
		List<Grad> gradovi = new ArrayList<>();
		while(rset.next()) {
			Grad grad = new Grad();
			int i = 1;
			grad.setId(rset.getInt(i++));
			grad.setNaziv(rset.getString(i++));
			grad.setPtt(rset.getInt(i++));
			gradovi.add(grad);
		}
		return gradovi;
	}
	
	private static List<Manifestacija> ispisSvihManifestacija(Statement stmt) throws SQLException {
		String sql = "SELECT m.id_manifestacija, m.naziv, m.broj_posetilaca, g.id_grad, g.naziv, g.ptt FROM manifestacije.manifestacija as m inner join grad as g on g.id_grad = m.grad_id_grad";
		ResultSet rset = stmt.executeQuery(sql);
		List<Manifestacija> manifestacije = new ArrayList<>();
		while(rset.next()) {
			Manifestacija manifestacija = new Manifestacija();
			int i = 1;
			manifestacija.setId(rset.getInt(i++));
			manifestacija.setNaziv(rset.getString(i++));
			manifestacija.setBrojPosetilaca(rset.getInt(i++));
			Grad grad = new Grad();
			grad.setId(rset.getInt(i++));
			grad.setNaziv(rset.getString(i++));
			grad.setPtt(rset.getInt(i++));
			manifestacija.setGrad(grad);
			manifestacije.add(manifestacija);
			
		}		
		return manifestacije;
	}

	public static void main(String[] args) throws SQLException, Exception {
		Scanner sc = new Scanner(System.in);
		Scanner scStr = new Scanner(System.in);
		File najvecaManifestacijaFile = new File("." + File.separator + "fileovi" + File.separator + "najveca-manifestacija.csv");

		try {
			// otvaranje konekcije, jednom na pocetku aplikacije
			ConnectionManager.open();
		} catch (Exception ex) {
			System.out.println("Neuspesna konekcija na bazu!");

			ex.printStackTrace();
			// kraj aplikacije
			return;
		}
		ResultSet rset = null;
		Statement stmt = ConnectionManager.getConnection().createStatement();
		int unos = 1;
		String sql;
		do {
			try {
				menu();
				unos = sc.nextInt();
				switch (unos) {
				case 1:
					List<Grad> gradovi = ispisSvihGradova(stmt);
					gradovi.forEach(System.out::println);
					break;
				case 2:
					List<Manifestacija> manifestacije = ispisSvihManifestacija(stmt);
					manifestacije.forEach(System.out::println);
					break;
				case 3:
					System.out.println("Unesite id manifestacije: ");
					int id = sc.nextInt();
					sql = "SELECT m.id_manifestacija, m.naziv, m.broj_posetilaca, g.id_grad, g.naziv, g.ptt FROM manifestacije.manifestacija as m inner join grad as g on g.id_grad = m.grad_id_grad where id_manifestacija =" + id;
					rset = stmt.executeQuery(sql);
					if(rset.next()) {
						Manifestacija manifestacija = new Manifestacija();
						int i = 1;
						manifestacija.setId(rset.getInt(i++));
						manifestacija.setNaziv(rset.getString(i++));
						manifestacija.setBrojPosetilaca(rset.getInt(i++));
						Grad grad = new Grad();
						grad.setId(rset.getInt(i++));
						grad.setNaziv(rset.getString(i++));
						grad.setPtt(rset.getInt(i++));
						manifestacija.setGrad(grad);
						System.out.println(manifestacija);
					}else {
						System.out.println("Manifestacija sa tim id-jem ne postoji!");
					}
					break;
				case 4:
					System.out.println("Unesite id manifestacije: ");
					id = sc.nextInt();
					sql = "SELECT * FROM manifestacije.manifestacija WHERE manifestacija.id_manifestacija = " + id;
					rset = stmt.executeQuery(sql);
					if(rset.next()) {
						System.out.println("Unesite naziv:");
						String naziv = scStr.nextLine();
						sql = "UPDATE manifestacije.manifestacija SET naziv = '" + naziv + "' WHERE (id_manifestacija = " + id + ");";
						stmt.executeUpdate(sql);
						System.out.println("Naziv uspesno promenjen!");
					}else {
						System.out.println("Manifestacija sa tim id-jem ne postoji!");
					}
					break;
				case 5:
					if(najvecaManifestacijaFile.exists()) {
						sql = "SELECT naziv,broj_posetilaca FROM manifestacije.manifestacija order by broj_posetilaca desc limit 1;";
						rset = stmt.executeQuery(sql);
						if(rset.next()) {
							Manifestacija manifestacija = new Manifestacija();
							manifestacija.setNaziv(rset.getString(1)); 
							manifestacija.setBrojPosetilaca(rset.getInt(2));
							Writer writter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(najvecaManifestacijaFile)));
							writter.write(manifestacija.toFileRepresentation());
							writter.close();
							System.out.println("Uspesno upisano u fajl!");						
						}else {
							System.out.println("Trenutno nema ni jedna manifestacija!");
						}
					}else {
						System.out.println("Pogresna putanja fajla!");
					}
					break;
				case 6:
					BufferedReader in = new BufferedReader(new FileReader(najvecaManifestacijaFile));
					String str;
					while((str = in.readLine()) != null) {
						System.out.println(str);
					}
					break;	
				case 7:
					System.out.println("Odaberite akciju: \n1)Unos manifestacije \n2)Izmena manifestacije \n3)Brisanje manifestacije");
					int odabir = sc.nextInt();
					if(odabir == 1) {
						System.out.println("Izaberite grad u kom se odrzava manifestacija:");
						gradovi = ispisSvihGradova(stmt);
						gradovi.forEach(System.out::println);
						int idGrada = sc.nextInt();
						boolean postoji = false;
						for (Grad grad : gradovi) {
							if(grad.getId() == idGrada) {
								postoji = true;
								break;
							}
						}
						if(postoji) {
							System.out.println("Unesite naziv:");
							String naziv = scStr.nextLine();
							System.out.println("Unesite broj posetilaca");
							int brojPosetilaca = sc.nextInt();
							sql = "INSERT INTO manifestacije.manifestacija (grad_id_grad, naziv, broj_posetilaca) VALUES (" + idGrada + ", '" + naziv + "', " + brojPosetilaca + ");";
							stmt.executeUpdate(sql);
							System.out.println("Manifestacija uspesno dodata!");
						}else {
							System.out.println("Grad sa unetim id-jem ne postoji!");
						}
						
					}else if (odabir == 2) {
						System.out.println("Izaberite mainifestaciju za izmenu:");
						manifestacije = ispisSvihManifestacija(stmt);
						manifestacije.forEach(System.out::println);
						int idManifestacije = sc.nextInt();
						Manifestacija odabranaManifestacija = null;
						for (Manifestacija man : manifestacije) {
							if(man.getId() == idManifestacije) {
								odabranaManifestacija = man;
								break;
							}
						}
						if(odabranaManifestacija != null) {
							System.out.println("Unesite naziv:");
							String naziv = scStr.nextLine();
							System.out.println("Unesite broj posetilaca:");
							int brojPosetilaca = sc.nextInt();
							sql = "UPDATE manifestacije.manifestacija SET naziv = '" + naziv + "', broj_posetilaca = " + brojPosetilaca + " WHERE (id_manifestacija = " + odabranaManifestacija.getId() + ");";
							stmt.executeUpdate(sql);
							System.out.println("Manifestacija uspesno izmenjena!");
						}else {
							System.out.println("Manifestacija sa tim id-jem ne postoji!");
						}
					}else if (odabir == 3) {
						System.out.println("Izaberite manifestaciju koju zelite da obrisete:");
						manifestacije = ispisSvihManifestacija(stmt);
						manifestacije.forEach(System.out::println);
						int idManifestacije = sc.nextInt();
						Manifestacija odabranaManifestacija = null;
						for (Manifestacija man : manifestacije) {
							if(man.getId() == idManifestacije) {
								odabranaManifestacija = man;
								break;
							}
						}
						if(odabranaManifestacija != null) {
							sql = "DELETE FROM manifestacije.manifestacija WHERE (id_manifestacija = " + odabranaManifestacija.getId() + ");";
							stmt.executeUpdate(sql);
							System.out.println("Manifestacija uspesno obrisana!");
						}else {
							System.out.println("Manifestacija sa tim id-jem ne postoji!");
						}
					}else {
						System.out.println("Pogresan unos!");
					}
					break;
				case 8 :
					System.out.println("Izmeni grad");
					break;
				case 0:
					System.out.println("Izlaz!");
					break;
				default:
					System.out.println("Pogresan unos!");
				}

			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Pogresan unos!");
				sc = new Scanner(System.in);
			}

		} while (unos != 0);

		try {
			ConnectionManager.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}



}
